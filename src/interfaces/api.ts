export interface TimeData {
  date: string
  confirmed: number
  deaths: number
  recovered: number
}

export type Country = TimeData[]

export interface TimeSeries {
  [key: string]: Country
}

export type GraphPoint = [ Date, number ]

export type GraphData = GraphPoint[]
