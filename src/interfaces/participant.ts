export default interface Participant {
  name: string
  wager: {
    confirmed: number
  }
}
