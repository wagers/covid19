module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === 'production'
    ? '/covid19/'
    : '/',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'Wagers: COVID-19'
        return args
      })
  },
  pwa: {
    workboxOptions: {
      skipWaiting: true
    }
  }
}
